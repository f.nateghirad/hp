<?php

use Illuminate\Database\Seeder;

class GraveyardsTableSeeder extends Seeder
{
    private $data = [
        ['title' => 'graveyard1'],
        ['title' => 'graveyard2'],
        ['title' => 'graveyard3']
    ];
    /**
     * @var \App\Repositories\GraveyardRepository
     */
    private $graveyardRepository;

    /**
     * GraveyardsTableSeeder constructor.
     * @param \App\Repositories\GraveyardRepository $graveyardRepository
     */
    public function __construct(\App\Repositories\GraveyardRepository $graveyardRepository)
    {
        $this->graveyardRepository = $graveyardRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $item) {
            $this->graveyardRepository->create($item);
        }
    }
}
