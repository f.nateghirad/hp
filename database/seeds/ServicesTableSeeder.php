<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    private $data = [
        ['title' => 'cleaning'],
        ['title' => 'flower'],
        ['title' => 'design']
    ];
    /**
     * @var \App\Repositories\ServiceRepository
     */
    private $serviceRepository;

    /**
     * ServicesTableSeeder constructor.
     * @param \App\Repositories\ServiceRepository $serviceRepository
     */
    public function __construct(\App\Repositories\ServiceRepository $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $item) {
            $this->serviceRepository->create($item);
        }
    }
}
