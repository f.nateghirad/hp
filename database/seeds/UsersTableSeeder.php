<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    private $data = [
        [
            'name' => 'customer',
            'email' => 'customer@hp.com',
            'password' => '123456'
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $item) {
            $user = new \App\Models\User();
            $user->name = $item['name'];
            $user->email = $item['email'];
            $user->password = bcrypt($item['password']);

            $user->save();
        }
    }
}
