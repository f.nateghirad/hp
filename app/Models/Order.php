<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
        'user_id',
        'operator_id',
        'service_id',
        'graveyard_id',
        'body',
        'grave_number'
    ];
}
