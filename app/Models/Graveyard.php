<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Graveyard extends Model
{
    protected $table = 'graveyards';

    protected $fillable = [
        'title',
        'status'
    ];
}
