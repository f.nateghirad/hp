<?php

namespace App\Repositories;


abstract class BaseRepository
{
    /**
     * @var \Illuminate\Database\Eloquent\Model;
     */
    protected $model;

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $entity = $this->model->create($data);

        return $entity;
    }

    /**
     * @param array $data
     * @param $model
     * @return boolean
     */
    public function update(array $data, $model)
    {
        try {
            return $this->isModelInstanceOrFetch($model)->update($data);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function fetchById($id)
    {
        return $this->model->find($id);
    }

    /**
     * @param $id
     * @param array $select
     * @param array $with
     * @return mixed
     */
    public function fetchByIdCustom($id, $select = [], $with = []){
        return $this->model->select($select)->with($with)->find($id);
    }

    /**
     * @param $id
     * @param array $select
     * @param array $with
     * @return mixed
     */
    public function fetchByIdCustomOrFail($id, $select = '*', $with = []){
        return $this->model->select($select)->with($with)->findOrFail($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function fetchByIdOrFail($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param array $where
     * @return mixed
     */
    public function fetchOrFail(array $where)
    {
        return $this->model->where($where)->firstOrFail();
    }

    /**
     * @param $object
     * @param bool $throwException
     * @return mixed
     */
    public function isModelInstanceOrFetch($object, $throwException = true)
    {
        if ($object instanceof $this->model) {
            return $object;
        }

        if ($throwException) {
            return $this->fetchByIdOrFail($object);
        } else {
            return $this->fetchById($object);
        }
    }

    /**
     * @param array $where
     * @param string $select
     * @param array $with
     * @param int $peerPage
     * @return mixed
     */
    public function fetch(array $where = [], $select = '*', $with = [], $peerPage = 20)
    {
        return $this->model->where($where)->select($select)->with($with)->paginate($peerPage);
    }

    /**
     * @param array $where
     * @param string $select
     * @param array $with
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function fetchAll(array $where = [], $select = '*', $with = [])
    {
        return $this->model->where($where)->select($select)->with($with)->get();
    }

    /**
     * @param $model
     * @return boolean
     */
    public function delete($model)
    {
        return $this->isModelInstanceOrFetch($model)->delete();
    }

    /**
     * @param array $where
     * @param array|null $with
     * @return mixed
     */
    public function fetchFirst(array $where = [], array $with = null)
    {
        $q =  $this->model->where($where);

        if ($with) {
            $q = $q->with($with);
        }

        return $q->first();
    }

    /**
     * @param array $where
     * @param array|null $with
     * @return mixed
     */
    public function fetchFirstOrFail(array $where, array  $with = null) {
        $q =  $this->model->where($where);

        if ($with) {
            $q = $q->with($with);
        }

        return $q->firstOrFail();
    }

    /**
     * @param string $orderBy
     * @param string $direction
     * @return mixed
     */
    public function all($orderBy = 'id', $direction = 'desc')
    {
        return $this->model->orderBy($orderBy, $direction)->get();
    }

    /**
     * @param array $where
     * @return int
     */
    public function count(array $where = [])
    {
        return $this->model->where($where)->count();
    }

    /**
     * @param $model
     * @return $this
     */
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return $this
     */
    public function freshModel()
    {
        $model = get_class($this->model);
        $this->model = new $model;
        return $this;
    }

}
