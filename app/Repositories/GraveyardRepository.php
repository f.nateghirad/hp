<?php


namespace App\Repositories;


use App\Models\Graveyard;

class GraveyardRepository extends BaseRepository
{
    public function __construct(Graveyard $graveyard)
    {
        $this->model = $graveyard;
    }
}
