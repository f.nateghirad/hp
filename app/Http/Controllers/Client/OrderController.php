<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Repositories\GraveyardRepository;
use App\Repositories\OrderRepository;
use App\Repositories\ServiceRepository;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * @var ServiceRepository
     */
    private $serviceRepository;
    /**
     * @var GraveyardRepository
     */
    private $graveyardRepository;
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * OrderController constructor.
     * @param ServiceRepository $serviceRepository
     * @param GraveyardRepository $graveyardRepository
     * @param OrderRepository $orderRepository
     */
    public function __construct(ServiceRepository $serviceRepository,
                                GraveyardRepository $graveyardRepository,
                                OrderRepository $orderRepository
    )
    {
        $this->serviceRepository = $serviceRepository;
        $this->graveyardRepository = $graveyardRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dd('submit successful');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $services = $this->serviceRepository->fetchAll();
        $graveyards = $this->graveyardRepository->fetchAll();

        return view('client.order.form', compact('services', 'graveyards'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        $inputs = $request->only([
            'service_id',
            'graveyard_id',
            'body',
            'grave_number']);
        $inputs['user_id'] = 1;
        $order = $this->orderRepository->create($inputs);
        dd($order);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
