@extends('master')

@section('title', 'create new order')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>

    @endif

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Panel title</h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post" action="{{ route('orders.store') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="service">service</label>
                    <select name="service_id" id="service" class="form-control">
                        @foreach($services as $service)
                            <option value="{{ $service->id }}">{{ $service->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="graveyard">graveyard</label>
                    <select name="graveyard_id" id="graveyard" class="form-control">
                        @foreach($graveyards as $graveyard)
                            <option value="{{ $graveyard->id }}">{{ $graveyard->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <textarea name="body" class="form-control" rows="3" placeholder="plz enter your order description"></textarea>
                </div>
                <div class="form-group">
                    <label for="grave_number" class="col-sm-2 control-label">grave number</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="grave_number" id="grave_number" placeholder="grave number">
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">submit</button>
                </div>
            </form>

        </div>


    </div>
@endsection
